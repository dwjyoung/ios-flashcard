//
//  FlashcardSetCollectionViewCell.swift
//  CS422L
//
//  Created by Daniel Young on 2/5/22.
//

import UIKit

class FlashcardSetCollectionViewCell: UICollectionViewCell {
    
    static let identifier = "FlashcardSetCollectionViewCell"
    
    let myLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.isHidden = false
        return label
    }()
    
    private let backView: UIView = {
        let backView = UIView()
        backView.backgroundColor = .darkGray
        backView.layer.cornerRadius = 8.0
        
        return backView
    }()
    
    private let middleView: UIView = {
        let middleView = UIView()
        middleView.backgroundColor = .blue
        middleView.layer.cornerRadius = 8.0
        return middleView
    }()
    
    private let frontView: UIView = {
        let frontView = UIView()
        frontView.backgroundColor = .systemGray
        frontView.layer.cornerRadius = 8.0
        return frontView
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(backView)
        contentView.addSubview(middleView)
        contentView.addSubview(frontView)
        frontView.addSubview(myLabel)
        contentView.clipsToBounds = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        myLabel.frame = CGRect(x:0,
                               y:0,
                               width: frontView.frame.size.width - 5 ,
                               height: frontView.frame.size.height - 5 )
        
        backView.frame = CGRect(x: 20,
                                y: 0,
                                width: contentView.frame.size.width/1.5,
                                height: contentView.frame.size.height/1.5)
        
        middleView.frame = CGRect(x: backView.frame.minX + 10,
                                  y: backView.frame.minY + 10,
                                width: contentView.frame.size.width/1.5,
                                height: contentView.frame.size.height/1.5)
        
        frontView.frame = CGRect(x: middleView.frame.minX + 10,
                                y: middleView.frame.minY + 10,
                                width: contentView.frame.size.width/1.5,
                                height: contentView.frame.size.height/1.5)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func configure(with flashcardSet: FlashcardSet){
        self.myLabel.text = flashcardSet.title
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        myLabel.text = nil
    }

}
