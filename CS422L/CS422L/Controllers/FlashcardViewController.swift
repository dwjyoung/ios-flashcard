//
//  FlashcardViewController.swift
//  CS422L
//
//  Created by Daniel Young on 2/7/22.
//

import UIKit
import CoreData

class FlashcardViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {

    var collectionView: UICollectionView!
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext


    var flashcardSet: FlashcardSet
    private var flashcards: [Flashcard]
    
    var editBool = false




    override func viewDidLoad() {
        super.viewDidLoad()
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        layout.itemSize = CGSize(width: view.frame.size.width/2.2,
                                 height: view.frame.size.height/4.4)
        let navBar = UINavigationBar(frame: CGRect(x: 0,
                                                  y: 0,
                                                  width: view.frame.size.width,
                                                  height: 44))
        guard let title = flashcardSet.title else{
            return
        }
        let navItem = UINavigationItem(title: title)
        navItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add,
                                                     target: self,
                                                     action: #selector(didTapAdd))

        navItem.leftBarButtonItem = UIBarButtonItem(title: "Study", style: .plain, target: self, action: #selector(didTapStudy))
        navBar.setItems([navItem], animated: true)
     
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)


        collectionView.register(
            FlashcardCollectionViewCell.self,
            forCellWithReuseIdentifier: FlashcardCollectionViewCell.identifier)

        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.addSubview(navBar)
        view.addSubview(collectionView)
        getAllItems()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionView?.frame = view.bounds
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    init(flashcardSetID: FlashcardSet, flashcards: [Flashcard]){
        self.flashcardSet = flashcardSetID
        self.flashcards = flashcards
        super.init(nibName: nil, bundle: nil)
        self.getAllItems()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

//    Button actions
    func getAllItems(){
        
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
        
    }

    func createItem(term: String, definition: String){
        let newItem = Flashcard(context: context)
        newItem.term = term
        newItem.definition = definition
        newItem.setValue(flashcardSet, forKey: "flashcardSet")
        flashcards.append(newItem)
        do {
            try self.context.save()
            self.getAllItems()
            viewDidLoad()
        }
        catch {
            //error
        }
    }

    func deleteItem(item: Flashcard, index: Int){
        context.delete(item)

        do {
            try context.save()
            flashcards.remove(at: index)
            self.getAllItems()
        }
        catch {
            //error
        }
    }

    func updateItem(item: Flashcard, newTerm: String, newDef: String){
        item.term = newTerm
        item.definition = newDef
        item.setValue(flashcardSet, forKey: "flashcardSet")

        do {
            try self.context.save()
            self.getAllItems()
        }
        catch {
            //error
        }

    }
    

    
    @objc func didTapAdd(){
        let alert = UIAlertController(title: "New Flashcard",
                                      message: "Enter new flashcard",
                                      preferredStyle: .alert)


        alert.addTextField(configurationHandler: { (termField) in
            termField.text = ""
            termField.placeholder = "term"
        })
        alert.addTextField(configurationHandler: { (definitionField) in
            definitionField.text = ""
            definitionField.placeholder = "definition"
        })
        alert.addAction(UIAlertAction(title: "Submit", style: .cancel, handler: { [weak self] _ in
            guard let termField = alert.textFields?[0],
                  let termText = termField.text,
                  let definitionField = alert.textFields?[1],
                  let definitionText = definitionField.text else{
                return
            }




            self?.createItem(term: termText, definition: definitionText)

        }))
        present(alert, animated: true)
    }
    
    @objc func didTapStudy(){
        let studyVc = StudySetViewController(flashcards: flashcards, flashcardSet: flashcardSet)
        present(studyVc, animated: true)
    }

    //collectionView

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let flashcard = flashcards[indexPath.row]
        
        
        let alert = UIAlertController(title: flashcard.term,
                                        message: flashcard.definition,
                                        preferredStyle: .alert)


            
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            
        alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: {[weak self] _ in
            self?.deleteItem(item: flashcard, index: indexPath.row)
        }))
            
        alert.addAction(UIAlertAction(title: "Edit", style: .default, handler: {[weak self] _ in
                
            let submitAlert = UIAlertController(title: "Update Flashcard",
                                                message: "Enter new flashcard values",
                                                preferredStyle: .alert)
                
            submitAlert.addTextField(configurationHandler: { (termField) in
                termField.text = flashcard.term
            })
            submitAlert.addTextField(configurationHandler: { (definitionField) in
                definitionField.text = flashcard.definition
            })
            submitAlert.addAction(UIAlertAction(title: "Submit", style: .cancel, handler: {[weak self] _ in
                    
                guard let termField = submitAlert.textFields?[0],
                        let termText = termField.text,
                        let definitionField = submitAlert.textFields?[1],
                        let definitionText = definitionField.text else{
                    return
                }
                self!.updateItem(item: flashcard,newTerm: termText, newDef: definitionText)
                    
            }))
            self!.present(submitAlert, animated: true)
        }))
            
            
        present(alert, animated: true)

    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return flashcards.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: FlashcardCollectionViewCell.identifier,
            for: indexPath) as! FlashcardCollectionViewCell

        cell.configure(with: flashcards[indexPath.row])

        return cell
    }


    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.size.width,
                      height: 100)
    }

}
