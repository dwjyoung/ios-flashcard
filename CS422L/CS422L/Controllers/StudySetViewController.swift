//
//  StudySetViewController.swift
//  CS422L
//
//  Created by Daniel Young on 2/14/22.
//

import UIKit

class StudySetViewController: UIViewController {
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var flashcards: [Flashcard]
    var firstFlashcard: Flashcard?
    var flashcardSet: FlashcardSet
    var showTermBool: Bool = false
    var indexInt: Int = 0
    var missedInt: Int = 0
    var correctInt: Int = 0
    var flashcardLabel = UILabel(frame: CGRect(x: 0,
                                      y: 0,
                                      width: 0,
                                      height: 0))
    var missed = UILabel(frame: CGRect(x: 0,
                                      y: 0,
                                      width: 0,
                                      height: 0))
    var correctLabel = UILabel(frame: CGRect(x: 0,
                                      y: 0,
                                      width: 0,
                                      height: 0))
    
    var touchGestureShowDeff = UITapGestureRecognizer(target: self, action: nil)
    var touchGestureShowTerm = UITapGestureRecognizer(target: self, action: nil)
    
    init(flashcards: [Flashcard], flashcardSet: FlashcardSet){
        self.flashcards = flashcards
        self.flashcardSet = flashcardSet
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private let flashcardView: UIView = {
        let alert = UIView()
        alert.backgroundColor = .white
        alert.layer.masksToBounds = true
        alert.layer.cornerRadius = 12
        return alert
    }()
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        flashcardView.frame = CGRect(x: 5,
                                     y: (view.frame.size.height/2) - 300,
                                     width: view.frame.size.width - 10,
                                     height: 500)
        
        flashcardLabel.frame = CGRect(x: 0,
                             y: flashcardView.frame.size.height/2,
                             width: flashcardView.frame.size.width,
                             height: 50)
        missed.frame = CGRect(x: (view.frame.size.width/2) - 90,
                              y: (view.frame.size.height/2) + 230,
                              width: 100,
                              height: 50)
        missed.text = "Missed: \(missedInt) "
        correctLabel.frame = CGRect(x: (view.frame.size.width/2) + 30,
                               y: (view.frame.size.height/2) + 230,
                               width: 100,
                               height: 50)
        correctLabel.text = "Correct: \(correctInt)"
        
        
        
        let swipeGestureUp = UISwipeGestureRecognizer(target: self, action: #selector(skip))
        let swipeGestureRight = UISwipeGestureRecognizer(target: self, action: #selector(correct))
        let swipeGestureLeft = UISwipeGestureRecognizer(target: self, action: #selector(wrong))
        touchGestureShowDeff.addTarget(self, action: #selector(showDefinition))
        touchGestureShowTerm.addTarget(self, action: #selector(showTerm))
        
        swipeGestureUp.direction = .up
        swipeGestureRight.direction = .right
        swipeGestureLeft.direction = .left
        
        flashcardView.addGestureRecognizer(swipeGestureUp)
        flashcardView.addGestureRecognizer(swipeGestureRight)
        flashcardView.addGestureRecognizer(swipeGestureLeft)
        flashcardView.addGestureRecognizer(touchGestureShowDeff)
        flashcardView.addGestureRecognizer(touchGestureShowTerm)

        if(flashcards.isEmpty){
            let finished = Flashcard()
            finished.term = "you are very smart"
            finished.definition = "maybe"
            flashcards.append(finished)
            let firstFlashcard: Flashcard? = flashcards[indexInt]
            guard let term = firstFlashcard?.term else {
                return
            }
            flashcardLabel.text = term
            viewDidLoad()
        } else {
            let firstFlashcard: Flashcard? = flashcards[indexInt]
            guard let term = firstFlashcard?.term else {
                return
            }
            flashcardLabel.text = term
        }
        
        flashcardLabel.textAlignment = .center
        
        flashcardView.addSubview(flashcardLabel)
        flashcardView.backgroundColor = .systemGray
        
        
        view.addSubview(missed)
        view.addSubview(correctLabel)
        view.addSubview(flashcardView)
        
    }
    func getAllItems(){
        
        DispatchQueue.main.async {
            self.viewDidLoad()
        }
        
    }
    
    @objc func skip(){
        let skipped = flashcards.remove(at: indexInt)
        flashcards.insert(skipped, at: flashcards.count-1)
        
        getAllItems()
        
    }
    
    @objc func correct(){
        if(flashcards.count == 1){
            flashcards.remove(at: indexInt)
            correctInt += 1
            let finished = Flashcard(context: context)
            finished.term = "you are very smart"
            finished.definition = "maybe"
            finished.setValue(flashcardSet, forKey: "flashcardSet")
            flashcards.append(finished)
            do {
                try self.context.save()
                self.getAllItems()
            }
            catch {
                //error
            }
        }else{
            flashcards.remove(at: indexInt)
            correctInt += 1
            do {
                try self.context.save()
                self.getAllItems()
            }
            catch {
                //error
            }
        }
        
 

        
    }
    @objc func wrong(){
        let skipped = flashcards.remove(at: indexInt)
        flashcards.insert(skipped, at: flashcards.count-1)
        missedInt += 1
        getAllItems()
    }
    
    func showTermActive(){
        touchGestureShowTerm.isEnabled = true
        touchGestureShowDeff.isEnabled = false
    }
    
    func showTermDeactive(){
        touchGestureShowTerm.isEnabled = false
        touchGestureShowDeff.isEnabled = true
    }
    
    @objc func showDefinition(){
        let flashcard: Flashcard? = flashcards[indexInt]
        guard let deffinition = flashcard?.definition else {
            return
        }
        
        showTermBool = true
        
        if(showTermBool == false){
            showTermDeactive()
        } else {
            showTermActive()
        }
        
        flashcardLabel.text = deffinition
    }
    
    @objc func showTerm(){
        let flashcard: Flashcard? = flashcards[indexInt]
        guard let term = flashcard?.term else {
            return
        }
        
        showTermBool = false
        
        if(showTermBool == false){
            showTermDeactive()
        } else {
            showTermActive()
        }
        
        flashcardLabel.text = term
    }
}
