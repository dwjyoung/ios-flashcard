//
//  ViewController.swift
//  CS422L
//
//  Created by Daniel Young on 2/5/21.
//
import CoreData
import SwiftUI
import UIKit

class MainViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
    
    var collectionView: UICollectionView!
    let context = (UIApplication.shared.delegate as!
        AppDelegate).persistentContainer.viewContext
    var flashcardSets: [FlashcardSet] = []
    var request = NSFetchRequest<NSFetchRequestResult>()

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        layout.itemSize = CGSize(width: view.frame.size.width/2.2,
                                 height: view.frame.size.height/4.4)
        let navBar = UINavigationBar(frame: CGRect(x: 0,
                                                  y: 0,
                                                  width: view.frame.size.width,
                                                  height: 44))
        
        let navItem = UINavigationItem(title:"MADFlashcards")
        navItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add,
                                                     target: self,
                                                     action: #selector(didTapAdd))
        navBar.setItems([navItem], animated: true)
        
        
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)

        
        collectionView.register(
            FlashcardSetCollectionViewCell.self,
            forCellWithReuseIdentifier: FlashcardSetCollectionViewCell.identifier)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.addSubview(navBar)
        view.addSubview(collectionView!)
        getAllItems()
    
    }
    

    
    func getAllItems(){
        do {
            let request = FlashcardSet.fetchRequest() as NSFetchRequest<FlashcardSet>
            
            
            
            self.flashcardSets = try context.fetch(request)
            
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            
        }
        catch {
            
        }

    }
    
    func createItem(title: String){
        let newItem = FlashcardSet(context: self.context)
        newItem.title = title
        

        do {
            try context.save()
            self.getAllItems()
            self.viewDidLoad()
        }
        catch {
            //error
        }
    }
    
    func deleteItem(item: FlashcardSet){
        self.context.delete(item)
        
        do {
            try self.context.save()
            self.getAllItems()
        }
        catch {
            //error
        }
    }
    
    func updateItem(item: FlashcardSet, newTitle: String){
        item.title = newTitle
        
        do {
            try self.context.save()
        }
        catch {
            //error
        }
        
    }
    
    @objc func didTapAdd(){
        let alert = UIAlertController(title: "New Set",
                                      message: "Enter new set",
                                      preferredStyle: .alert)
        alert.addTextField(configurationHandler: nil)
        alert.addAction(UIAlertAction(title: "Submit", style: .cancel, handler: { [weak self] _ in
            guard let field = alert.textFields?.first, let text = field.text, !text.isEmpty else{
                return
            }
            self?.createItem(title: text)
            
        }))
        present(alert, animated: true)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionView?.frame = view.bounds
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    //collectionView
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let id = flashcardSets[indexPath.item]
        let flashcards = flashcardSets[indexPath.item].flashcards
        
        let flashcardVC = FlashcardViewController(flashcardSetID: id, flashcards: flashcards)
        

        present(flashcardVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return flashcardSets.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let flashcardSet = flashcardSets[indexPath.item]
        let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: FlashcardSetCollectionViewCell.identifier,
            for: indexPath) as! FlashcardSetCollectionViewCell
        
        cell.configure(with: flashcardSet)
        
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.size.width,
                      height: 100)
    }
    
    
    
}

