//
//  FlashcardSet+CoreDataProperties.swift
//  CS422L
//
//  Created by Daniel Young on 2/22/22.
//
//

import Foundation
import CoreData


extension FlashcardSet {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<FlashcardSet> {
        return NSFetchRequest<FlashcardSet>(entityName: "FlashcardSet")
    }

    @NSManaged public var title: String?
    @NSManaged public var flashcards: [Flashcard]

}

// MARK: Generated accessors for flashcards
extension FlashcardSet {

    @objc(addFlashcardsObject:)
    @NSManaged public func addToFlashcards(_ value: Flashcard)

    @objc(removeFlashcardsObject:)
    @NSManaged public func removeFromFlashcards(_ value: Flashcard)

    @objc(addFlashcards:)
    @NSManaged public func addToFlashcards(_ values: NSSet)

    @objc(removeFlashcards:)
    @NSManaged public func removeFromFlashcards(_ values: NSSet)

}

extension FlashcardSet : Identifiable {

}
