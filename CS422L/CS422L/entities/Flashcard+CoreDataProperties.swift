//
//  Flashcard+CoreDataProperties.swift
//  CS422L
//
//  Created by Daniel Young on 2/22/22.
//
//

import Foundation
import CoreData


extension Flashcard {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Flashcard> {
        return NSFetchRequest<Flashcard>(entityName: "Flashcard")
    }

    @NSManaged public var definition: String?
    @NSManaged public var term: String?
    @NSManaged public var flashcardSet: FlashcardSet?

}

extension Flashcard : Identifiable {

}
